var Engine = {
    process : function(template, names) {
        var regexp,
            param;

        for (param in names) {
            regexp = new RegExp('\\$\\{' + param + '\\}', 'g');
            template = template.replace(regexp, names[param]);
        }
        return template;
    }
};

Engine.process('some ${sad} ${some}', {sad:'adsad', some:'adfadd'});