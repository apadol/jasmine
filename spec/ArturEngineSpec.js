describe('Template Engine Parser', function () {
	it("Replaces tags ${name} with given value", function(){
		var template = "Zmienna 1: ${a}, Zmienna 2: ${b}",
			variables = {a: "a", b : "b"},
			result;

		result = Engine.process(template, variables);
		expect(result).toEqual("Zmienna 1: a, Zmienna 2: b")
	});
});