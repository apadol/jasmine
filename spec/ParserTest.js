describe('Parser', function () {

    var parser;

    beforeEach(function () {
        parser = new Parser();
    });

    describe('parse', function () {
        it('should replace all special tags in template with given object', function () {
            var template = "Some ${name} and ${anotherName}",
                nameList = {
                    name : 'SOMEONE',
                    anotherName : 'ANOTHER'
                },
                expectedString = "Some SOMEONE and ANOTHER";

            expect(parser.parse(template, nameList)).toEqual(expectedString);
        });
        it('should return error if given parameters are incomplete', function () {
            expect(function () {
                parser.parse('some');
            }).toThrow();
            expect(function () {
                parser.parse();
            }).toThrow();
            expect(function () {
                parser.parse('${name}', {another : ''});
            }).toThrow();
        });
    });
});